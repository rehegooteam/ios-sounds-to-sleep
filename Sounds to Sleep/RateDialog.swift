//
//  RateDialog.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 22/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class RateDialog: NSObject {

    public static func showRateDialog(_ vc : UIViewController) {
        
        let message = NSLocalizedString("rate_message", comment: "")
        let title = "\(NSLocalizedString("rate", comment: "")) \(Bundle.main.object(forInfoDictionaryKey: "CFBundleDisplayName") as! String)"
        let later = NSLocalizedString("rate_later", comment: "")
        
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertActionYES = UIAlertAction(title: title, style: .default) { action -> Void in
            
            rateApp(appId: RehegooConstants.APP_ID.description) { success in
                print("RateApp \(success)")
            }
            
            alertController.dismiss(animated: true, completion: nil)
        }
        
        let alertActionLATER = UIAlertAction(title: later, style: .default) { action -> Void in
            alertController.dismiss(animated: true, completion: nil)
        }
        
        alertController.addAction(alertActionLATER)
        alertController.addAction(alertActionYES)
        
        vc.present(alertController, animated: true, completion: nil)
    }
    
    public static func rateApp(appId: String, completion: @escaping ((_ success: Bool)->())) {
        guard let url = URL(string : "itms-apps://itunes.apple.com/app/" + "id" + appId) else {
            completion(false)
            return
        }
        guard #available(iOS 10, *) else {
            completion(UIApplication.shared.openURL(url))
            return
        }
        UIApplication.shared.open(url, options: convertToUIApplicationOpenExternalURLOptionsKeyDictionary([:]), completionHandler: completion)
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToUIApplicationOpenExternalURLOptionsKeyDictionary(_ input: [String: Any]) -> [UIApplication.OpenExternalURLOptionsKey: Any] {
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (UIApplication.OpenExternalURLOptionsKey(rawValue: key), value)})
}
