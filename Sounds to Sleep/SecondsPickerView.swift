//
//  SecondsPickerView.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 20/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SecondsPickerView: TimePickerView {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        numberOfRows = 60
        pickerType = .Seconds
        setInitialValue(animate: false)
    }
}
