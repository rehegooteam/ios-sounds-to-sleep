//
//  TimerManager.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 20/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class TimerManager: NSObject {
    public static let shared : TimerManager = TimerManager()
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    
    var isOn : Bool {
        get {
            return timer != nil ? (timer?.isValid)! : false
        }
    }
    
    var currentSeconds : Int
    
    private var timer : Timer?
    
    override init() {
        currentSeconds = RehegooConfig.getTotalSeconds()
        super.init()
    }
    
    func onStartTapped() {
        isOn ? stopTimer() : startTimer()
    }
    
    func startTimer() {
        currentSeconds = RehegooConfig.getTotalSeconds()
        
        guard currentSeconds != 0 else {
            return
        }
        
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: {
            UIApplication.shared.endBackgroundTask(self.backgroundTaskIdentifier!)
        })
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(onTick), userInfo: nil, repeats: true)
        timer?.fire()
        
        onTimerStateChanged()
    }
    
    func stopTimer() {
        timer?.invalidate()
        
        onTimerStateChanged()
    }
    
    @objc func onTick() {
        if(currentSeconds <= 0) {
            stopTimer()
            SoundsManager.shared.stopAllSounds()
            NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIMER_ENDED), object: nil)
            return
        }
        
        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIMER_SECONDS_CHANGED), object: currentSeconds)
        currentSeconds = currentSeconds - 1
        print("onTimerTick with seconds: \(currentSeconds)")
    }
    
    func onTimerStateChanged() {
        
        print(isOn ? "Timer started" : "Timer stopped")
        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIMER_STATE_CHANGED), object: nil)
    }
}
