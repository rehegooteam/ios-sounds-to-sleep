//
//  LocalizedUILabel.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 22/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class LocalizedUILabel: UILabel {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        text = NSLocalizedString(text!, comment: "")
    }
}
