//
//  LeftImageUIButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 14/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class LeftImageUIButton: UIButton {
    
    var buttonImageView: UIImageView?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addImage()
    }
    
    func addImage() {
        buttonImageView = UIImageView(frame: CGRect.init(x: 0, y: 0, width: self.frame.height, height: self.frame.height))
        
        buttonImageView?.backgroundColor = UIColor.clear
        
        buttonImageView?.contentMode = .center
        
        addSubview(buttonImageView!)
    }
    
    func setButtonImage(imageName: String) {
        let buttonImage: UIImage
        
        buttonImage = UIImage.init(imageLiteralResourceName: imageName)
        
        buttonImageView?.image = buttonImage
    }
}
