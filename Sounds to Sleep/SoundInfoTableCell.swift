//
//  SoundInfoTableCell.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 13/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SoundInfoTableCell: UITableViewCell {
    @IBOutlet weak var soundNameLbl: UILabel!
    @IBOutlet weak var soundImage: UIImageView!
    
    public static let CELL_ID : String = "soundinfocellid";
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupViewWith(sound: Sound) {
        self.tag = sound.index
        backgroundColor = UIColor.clear
        
        soundNameLbl.text = sound.name
        soundImage.image = UIImage(named: sound.iconName)
        
        setupCheckmark()
    }
    
    func setupCheckmark() {
        accessoryType = RehegooConfig.isSoundSelected(soundIndex: tag) ? .checkmark : .none
    }
}
