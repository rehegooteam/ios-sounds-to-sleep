//
//  RestorePurchasesButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 13/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class RestorePurchasesButton: UIButton {
    
    var attrs = [
        convertFromNSAttributedStringKey(NSAttributedString.Key.underlineStyle) : NSUnderlineStyle.single.rawValue
    ] as [String : Any]
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setAttributedTitle(NSAttributedString(string: NSLocalizedString("restore_purchases", comment: ""), attributes: convertToOptionalNSAttributedStringKeyDictionary(attrs)), for: .normal)
        
        self.addTarget(self, action: #selector(onTouched), for: .touchDown)
    }
    
    @objc func onTouched() {
        Animate.bounce(view: self, type: .Light);
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromNSAttributedStringKey(_ input: NSAttributedString.Key) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertToOptionalNSAttributedStringKeyDictionary(_ input: [String: Any]?) -> [NSAttributedString.Key: Any]? {
	guard let input = input else { return nil }
	return Dictionary(uniqueKeysWithValues: input.map { key, value in (NSAttributedString.Key(rawValue: key), value)})
}
