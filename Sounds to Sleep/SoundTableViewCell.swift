//
//  SoundTableViewCell.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 07/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SoundTableViewCell: UITableViewCell {
    public static let CELL_HEIGHT : CGFloat = 70;
    public static let CELL_ID : String = "soundCellId";
    
    static let settingsImage = UIImage(named: "settings")
    static let settingsFillImage = UIImage(named: "settings_fill")
    static let playImage = UIImage(named: "play")
    static let pauseImage = UIImage(named: "pause")
    static let playBackgroundColor = UIColor(hexString: "#77C5F5")
    
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var settingsBtn: UIButton!
    @IBOutlet weak var removeBtn: UIButton!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var cellBackground: UIView!
    @IBOutlet weak var soundImage: UIImageView!

    public var soundCellDelegate : SoundCellDelegate?
    
    private var soundIndex: Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        cellBackground.layer.cornerRadius = (SoundTableViewCell.CELL_HEIGHT / 2) - 8
        cellBackground.layer.borderWidth = 1;
        cellBackground.layer.borderColor = UIColor.white.cgColor
        cellBackground.backgroundColor = UIColor.clear
    }
    
    public func setupView(with sound: Sound, and cellIndex: Int, settingsOpened: Bool) {
        backgroundColor = UIColor.clear
        
        removeBtn.isHidden = (RehegooConfig.isPremium()) ? false : true
        
        nameLbl.text = sound.name
        soundImage.image = UIImage(named: sound.iconName)
        
        soundIndex = sound.index
        
        tag = cellIndex
        
        settingsBtn.setImage(settingsOpened ? SoundTableViewCell.settingsFillImage : SoundTableViewCell.settingsImage, for: .normal)

        setupWithSoundController()
    }
    
    func setupWithSoundController() {
        let isSoundPlaying : Bool = SoundsManager.shared.isSoundPlaying(soundIndex: soundIndex!)
        
        playBtn.setImage(isSoundPlaying ? SoundTableViewCell.pauseImage : SoundTableViewCell.playImage, for: .normal)
        
        cellBackground.backgroundColor = isSoundPlaying ? SoundTableViewCell.playBackgroundColor : UIColor.clear
    }

    @IBAction func onPlayTapped(_ sender: UIButton) {
        Animate.bounce(view: sender, type: .Normal)
        
        SoundsManager.shared.onPlayTapped(soundIndex: soundIndex!)
        
        setupWithSoundController()
    }
    
    @IBAction func onRemoveTapped(_ sender: UIButton) {
        Animate.bounce(view: sender, type: .Normal)
        
        RehegooConfig.sound(isSelected: false, soundIndex: soundIndex!)
    }
    
    @IBAction func onSettingsTapped(_ sender: UIButton) {
        Animate.bounce(view: sender, type: .Normal)
        
        let settingsImage : String = (soundCellDelegate?.onSettingsTapped(cellIndex: tag, soundIndex: soundIndex!))! ? "settings_fill" : "settings"
        
        settingsBtn.setImage(UIImage(named: settingsImage), for: .normal)
    }
}
