//
//  BrigthnessButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 04/07/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

enum BrightnessState: Float {
    case one = 0.25
    case two = 0.5
    case three = 0.75
    case four = 1.0
}

class BrigthnessButton: UIButton {
    
    lazy var currentBrightnessState = BrightnessState(rawValue: RehegooConfig.getBrightnessLevel())!

    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupBrightnessLevel()
    }
    
    func setupBrightnessLevel() {
        
        switch currentBrightnessState {
        case .one:
            self.setImage(UIImage.init(named: "brightness1"), for: .normal)
            UIScreen.main.brightness = CGFloat(BrightnessState.one.rawValue)
        case .two:
            self.setImage(UIImage.init(named: "brightness2"), for: .normal)
            UIScreen.main.brightness = CGFloat(BrightnessState.two.rawValue)
        case .three:
            self.setImage(UIImage.init(named: "brightness3"), for: .normal)
            UIScreen.main.brightness = CGFloat(BrightnessState.three.rawValue)
        case .four:
            self.setImage(UIImage.init(named: "brightness4"), for: .normal)
            UIScreen.main.brightness = CGFloat(BrightnessState.four.rawValue)
        }
    }
    
    func changeBrightnessStatus() {
        switch currentBrightnessState {
        case .one:
            RehegooConfig.setBrightnessLevel(brightnessLevel: BrightnessState.two.rawValue)
            currentBrightnessState = .two
        case .two:
            RehegooConfig.setBrightnessLevel(brightnessLevel: BrightnessState.three.rawValue)
            currentBrightnessState = .three
        case .three:
            RehegooConfig.setBrightnessLevel(brightnessLevel: BrightnessState.four.rawValue)
            currentBrightnessState = .four
        case .four:
            RehegooConfig.setBrightnessLevel(brightnessLevel: BrightnessState.one.rawValue)
            currentBrightnessState = .one
        }
        
        setupBrightnessLevel()
    }
}
