//
//  RehegooVC.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import GoogleMobileAds

class RehegooVC: UIViewController, GADBannerViewDelegate {

    @IBOutlet weak var rehegooGADBanner: RehegooGADBanner!
    @IBOutlet weak var constraintGADBannerWidth: NSLayoutConstraint!
    @IBOutlet weak var constraintGADBannerHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintGADBannerBottom: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addObservers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        RehegooConfig.isPremium() ? hideBanner() : initializeGADBanner()
    }
    
    func addObservers() {
        guard !RehegooConfig.isPremium() else {
            return;
        }
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(hideBanner),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED),
            object: nil)
    }
    
    @objc func hideBanner() {
        rehegooGADBanner?.isHidden = true
        rehegooGADBanner?.delegate = nil
        rehegooGADBanner = nil
        
        constraintGADBannerWidth?.constant = 0
        constraintGADBannerHeight?.constant = 0
        constraintGADBannerBottom?.constant = 0
        
        view.updateConstraints()
    }
    
    func initializeGADBanner() {
        guard !RehegooConfig.isPremium() else {
            print("adBanner can not be processed! (Premium Purchased)")
            return;
        }
        
        rehegooGADBanner.delegate = self
        
        constraintGADBannerWidth.constant = rehegooGADBanner.GADBannerWidth
        constraintGADBannerHeight.constant = rehegooGADBanner.GADBannerHeight
        constraintGADBannerBottom.constant = -rehegooGADBanner.GADBannerHeight
        
        view.updateConstraints()
        
        rehegooGADBanner.initialize(vc: self)
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView) {
        print("GADBanner:didReceiveAd")
        
        guard !RehegooConfig.isPremium() else {
            print("adBanner can not be processed! (Premium Purchased)")
            return;
        }
        
        bannerView.isHidden = false
        
        constraintGADBannerBottom.constant = 0
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
    
    func adView(_ bannerView: GADBannerView, didFailToReceiveAdWithError error: GADRequestError) {
        print("GADBanner:didFailToReceiveAdWithError: \(error.localizedDescription)")
        
        guard !RehegooConfig.isPremium() else {
            print("adBanner can not be processed! (Premium Purchased)")
            return;
        }
        
        bannerView.isHidden = true
        
        constraintGADBannerBottom.constant = -rehegooGADBanner.GADBannerHeight
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
