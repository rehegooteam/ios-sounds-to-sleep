//
//  ViewController.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 07/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class ViewController: RehegooVC {

    @IBOutlet weak var constraintLogoTraining: NSLayoutConstraint!
    
    @IBOutlet weak var freeSoundOfTheDayView: UIView!
    @IBOutlet weak var premiumBtn: PremiumButton!
    @IBOutlet weak var timerBtn: UIButton!
    
    var timerIsOpenned = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        addNotificationObservers()
        
        NavigationController.shared.initialize(withStoryboard: self.storyboard!)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupPremiumViews()
        setTransparentNavigationBar()
    }
    
    func addNotificationObservers() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(setupPremiumViews),
                                               name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onTimerStateChanged),
                                               name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_STATE_CHANGED),
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onTimerSecondsChanged(notification:)),
                                               name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_SECONDS_CHANGED),
                                               object: nil)
    }
    
    @objc func onTimerStateChanged() {
        title = nil
    }
    
    @objc func onTimerSecondsChanged(notification: Notification) {
        let currentTimerSeconds : Int = notification.object as! Int
        
        let hours = currentTimerSeconds.toHours()
        let minutes = currentTimerSeconds.toMinutes()
        let seconds = currentTimerSeconds.toSeconds()
        
        if(hours != 0) {
            title = "\(String(format: "%02d", hours)):\(String(format: "%02d", minutes)):\(String(format: "%02d", seconds))"
        } else if (minutes != 0) {
            title = "\(String(format: "%02d", minutes)):\(String(format: "%02d", seconds))"
        } else {
            title = "\(seconds)"
        }
    }
    
    @objc func setupPremiumViews() {
        
        setupTimer()
        
        setupFreeSongOfTheDay()
    }
    
    func setupFreeSongOfTheDay() {
        if(RehegooConfig.isPremium()) {
            freeSoundOfTheDayView.removeFromSuperview()
        }
    }
    
    func setupTimer() {
        timerBtn.isUserInteractionEnabled = RehegooConfig.isPremium()
        
        timerBtn.isUserInteractionEnabled ? timerBtn.setImage(UIImage(named: "timer"), for: .normal) : timerBtn.setImage(nil, for: .normal)
    }
    
    func setTransparentNavigationBar() -> Void {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        // Sets shadow (line below the bar) to a blank image
        self.navigationController?.navigationBar.shadowImage = UIImage()
        // Sets the translucent background color
        self.navigationController?.navigationBar.backgroundColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 0.0)
        // Set translucent. (Default value is already true, so this can be removed if desired.)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
    @IBAction func onBrightnessTapped(_ sender: BrigthnessButton) {
        sender.changeBrightnessStatus()
    }
    
    @IBAction func onPremiumTapped(_ sender: PremiumButton) {
        RehegooConfig.isPremium() ?
            NavigationController.shared.navigateToSoundsPicker(fromVC: self) :
            NavigationController.shared.navigateToPurchase(fromVC: self)
    }
    
    @IBAction func onTimerTapped(_ sender: Any) {
        Animate.bounce(view: sender as! UIView, type: .Normal)
        
        timerIsOpenned = !timerIsOpenned
        
        timerBtn.setImage(UIImage(named: timerIsOpenned ? "timer_fill" : "timer"), for: .normal)
        
        constraintLogoTraining.constant = timerIsOpenned ? -self.view.frame.width : 0
        
        UIView.animate(withDuration: 0.25) {
            self.view.layoutIfNeeded()
        }
    }
}

