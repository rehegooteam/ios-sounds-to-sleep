//
//  LogoVC.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 08/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class LogoVC: UIViewController {

    @IBOutlet weak var logoView: UIView!
    @IBOutlet weak var moonImage: UIImageView!
    @IBOutlet weak var starsLayer1: UIImageView!
    @IBOutlet weak var starsLayer2: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(removeMoonAnimation), name: UIApplication.didEnterBackgroundNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(startMoonAnimation), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addParallaxToView(vw: logoView, amount: 20)
        addParallaxToView(vw: starsLayer1, amount: 10)
        addParallaxToView(vw: starsLayer2, amount: 10)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        startMoonAnimation()
    }
    
    @objc func startMoonAnimation() {
        Animate.startInfiniteRotation(view: moonImage)
        Animate.startInfiniteAlphaAnimation(view: starsLayer1, duration: 1.0, delay: 0, alpha: 0.3)
        Animate.startInfiniteAlphaAnimation(view: starsLayer2, duration: 1.2, delay: 0, alpha: 0.3)
    }
    
    @objc func removeMoonAnimation() {
        Animate.stopInfiniteRotation(view: moonImage)
    }
    
    func addParallaxToView(vw: UIView, amount: Int) {
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -amount
        horizontal.maximumRelativeValue = amount
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -5
        vertical.maximumRelativeValue = 5
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        vw.addMotionEffect(group)
    }
}
