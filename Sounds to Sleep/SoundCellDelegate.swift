//
//  SoundCellDelegate.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 15/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

protocol SoundCellDelegate {
    func onSettingsTapped(cellIndex : Int, soundIndex: Int) -> Bool
}
