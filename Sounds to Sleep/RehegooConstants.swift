//
//  RehegooUtil.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class RehegooConstants: NSObject {
    
    public static let ON_PREMIUM_PURCHASED = "onpremiumpurchasednotificationname"
    public static let ON_VOLUME_CHANGED = "onvolumechangednotificationname"
    public static let ON_TIME_CHANGED = "ontimechangednotificationname"
    public static let ON_TIMER_STATE_CHANGED = "ontimerstatechangednotificationame"
    public static let ON_TIMER_SECONDS_CHANGED = "ontimersecondschangednotificationname"
    public static let ON_TIMER_ENDED = "ontimerendednotificationname"
    
    public static let NUMBER_OF_FREE_SOUNDS = 10
    
    public static let TEST_DEVICES = [ "099b9f7573830b389c22dd60d3213889",
                                       "802ae4c4053c4712988e5888765dafeb",
                                       "95d562803279be26b72a533362806a90" ]
    
    public static let AD_MOB_PUBLISHER_ID = "pub-4282233236322618"
    
    // MARK: - Advertisements - To change!
    public static let GAD_BANNER_ID = "ca-app-pub-4282233236322618/9583024687"
    public static let GAD_INTERSTITIAL_ID = "ca-app-pub-4282233236322618/2059757882"
    
    // MARK: - In App Purchase - To change!
    public static let PREMIUM_PRODUCT_ID = "com.rehegoo.SoundstoSleep.premium"
    
    
    public static let APP_ID = 1205440159
}
