//
//  Sound.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 08/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class Sound: NSObject {
    
    public let index : Int
    public let name : String
    public var iconName : String
    
    required init(soundIndex: Int, soundIconName: String) {
        index = soundIndex
        iconName = soundIconName
        name = NSLocalizedString("sound\(index)", comment: "")
        
        super.init()
    }
}
