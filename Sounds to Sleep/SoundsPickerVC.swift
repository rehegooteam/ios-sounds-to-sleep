//
//  SoundsPickerVC.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 13/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SoundsPickerVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func rateApp(_ sender: Any) {
        Animate.bounce(view: sender as! UIView, type: .Normal)
        
        RateDialog.showRateDialog(self)
    }
}
