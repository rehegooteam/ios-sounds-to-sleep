//
//  SoundsPickerTableVC.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 13/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SoundsPickerTableVC: UITableViewController {
    
    override func awakeFromNib() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSoundSelectedStateChanged(notification:)), name: NSNotification.Name(rawValue: RehegooConfig.SELECTED_SOUND_KEY), object: nil)
        
//         NotificationCenter.default.addObserver(self, selector: #selector(onPremiumPurchased), name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED), object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
//    func onPremiumPurchased() {
//        tableView.reloadSections([0], with: .automatic)
//    }

    // MARK: - Table view data source

    @objc func onSoundSelectedStateChanged(notification: Notification) {
//        let songIndex : Int = notification.object as! Int
        
        tableView.reloadData()
        
//        tableView.reloadRows(at: [IndexPath(row: songIndex - 1, section: 0)], with: .automatic)
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return SoundsRepository.shared.getNumberOfSounds()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : SoundInfoTableCell = tableView.dequeueReusableCell(withIdentifier: SoundInfoTableCell.CELL_ID, for: indexPath) as! SoundInfoTableCell
        
        let sound = SoundsRepository.shared.sounds[indexPath.row]
        
        cell.setupViewWith(sound: sound)
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let soundIndex = SoundsRepository.shared.sounds[indexPath.row].index
        
        print("Sound Index: ", soundIndex)
        
        RehegooConfig.sound(isSelected: !RehegooConfig.isSoundSelected(soundIndex: soundIndex),
                            soundIndex: soundIndex)
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
