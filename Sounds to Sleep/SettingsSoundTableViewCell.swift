//
//  SettingsSoundTableViewCell.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 15/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SettingsSoundTableViewCell: UITableViewCell {

    @IBOutlet weak var alignBtn: UIButton!
    @IBOutlet weak var volumeSld: UISlider!
    @IBOutlet weak var earLeftSld: UISlider!
    @IBOutlet weak var earRightSld: UISlider!
    
    public static let CELL_HEIGHT : CGFloat = 80;
    public static let CELL_ID : String = "settingsSoundCellid";
    
    private var soundIndex : Int?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupHeadsetViews() {
        let headsetPluggedIn = RehegooConfig.headsetPluggedIn()
        
        DispatchQueue.main.async {
            self.alignBtn.isEnabled = headsetPluggedIn
            self.earLeftSld.isEnabled = headsetPluggedIn
            self.earRightSld.isEnabled = headsetPluggedIn
        }
    }
    
    public func setupView(with sound: Sound) {
        backgroundColor = UIColor.clear
        
        soundIndex = sound.index
        tag = soundIndex!
        
        volumeSld.setValue(RehegooConfig.getSoundVolume(soundIndex: sound.index), animated: false)
        earLeftSld.setValue(RehegooConfig.getSoundLeftPanValue(soundIndex: sound.index), animated: false)
        earRightSld.setValue(RehegooConfig.getSoundRightPanValue(soundIndex: sound.index), animated: false)
        
        setupHeadsetViews()
    }
    
    @IBAction func onAlignTapped(_ sender: Any) {
        RehegooConfig.setSoundLeftPanValue(soundIndex: soundIndex!, value: earRightSld.value)
        
        earLeftSld.setValue(earRightSld.value, animated: true)
    }
    
    @IBAction func onVolumeChanged(_ sender: Any) {
        RehegooConfig.setSoundVolume(soundIndex: soundIndex!, volume: volumeSld.value)
    }
    
    @IBAction func onLeftEarVolumeChanged(_ sender: Any) {
        RehegooConfig.setSoundLeftPanValue(soundIndex: soundIndex!, value: earLeftSld.value)
    }
    
    @IBAction func onRightEarVolumeChanged(_ sender: Any) {
        RehegooConfig.setSoundRightPanValue(soundIndex: soundIndex!, value: earRightSld.value)
    }
}
