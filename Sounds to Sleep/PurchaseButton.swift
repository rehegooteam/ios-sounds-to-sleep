//
//  PurchaseButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 10/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class PurchaseButton: LeftImageUIButton {

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
        self.setTitle(NSLocalizedString("premium", comment: ""), for: .normal)
        self.addTarget(self, action: #selector(onTouched), for: .touchDown)
        setButtonImage(imageName: "diamond")
    }
    
    func setupView() -> Void {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    @objc func onTouched() {
        Animate.bounce(view: self, type: .Light);
    }
}
