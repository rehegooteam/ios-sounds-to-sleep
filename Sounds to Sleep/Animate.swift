//
//  Animate.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 08/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

enum BounceType {
    case Light
    case Normal
}

class Animate: NSObject {
    
    public static let kRotationAnimationKey = "com.myapplication.rotationanimationkey"
    
    public static func bounce(view: UIView, type: BounceType) {
        
        switch type {
        case .Light:
            view.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        case .Normal:
            view.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }
        
        UIView.animate(withDuration: 2.0,
                       delay: 0,
                       usingSpringWithDamping: CGFloat(0.20),
                       initialSpringVelocity: CGFloat(6.0),
                       options: UIView.AnimationOptions.allowUserInteraction,
                       animations: {
                        view.transform = CGAffineTransform.identity
        },
                       completion: { Void in()  }
        )
    }
    
    public static func startInfiniteAlphaAnimation(view: UIView, duration: TimeInterval, delay: TimeInterval, alpha: CGFloat) {
        
        view.alpha = 1.0
        
        UIView.animate(withDuration: duration, delay: delay, options: [.repeat, .autoreverse], animations: {
                view.alpha = alpha
        }, completion: nil)
    }
    
    public static func startInfiniteRotation(view: UIView) {
        if view.layer.animation(forKey: kRotationAnimationKey) == nil {
            let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
            
            rotationAnimation.fromValue = 0.0
            rotationAnimation.toValue = Float(Double.pi * 2.0)
            rotationAnimation.duration = 35
            rotationAnimation.repeatCount = Float.infinity
            
            view.layer.add(rotationAnimation, forKey: kRotationAnimationKey)
        }
    }
    
    public static func stopInfiniteRotation(view: UIView) {
        if view.layer.animation(forKey: kRotationAnimationKey) != nil {
            view.layer.removeAnimation(forKey: kRotationAnimationKey)
        }
    }
}
