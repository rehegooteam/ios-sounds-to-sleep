//
//  SoundsPlayer.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 16/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import AVFoundation

class SoundsManager: NSObject {
    public static let shared : SoundsManager = SoundsManager()
    
    private var soundsPlayers : [SoundPlayer] = Array<SoundPlayer>()
    
    required override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSoundSelectedStateChanged(notification:)), name: NSNotification.Name(rawValue: RehegooConfig.SELECTED_SOUND_KEY), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onSoundVolumeChanged(notification:)), name: NSNotification.Name(rawValue: RehegooConstants.ON_VOLUME_CHANGED), object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleRouteChange(notification:)),
                                               name: AVAudioSession.routeChangeNotification,
                                               object: nil)
    }
    
    @objc func handleRouteChange(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let reasonRaw = userInfo[AVAudioSessionRouteChangeReasonKey] as? NSNumber,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonRaw.uintValue)
            else { fatalError("Strange... could not get routeChange") }
        switch reason {
        case .oldDeviceUnavailable:
            print("oldDeviceUnavailable")
            onHeadsetStateChanged()
        case .newDeviceAvailable:
            print("newDeviceAvailable")
            onHeadsetStateChanged()
        case .routeConfigurationChange:
            print("routeConfigurationChange")
            onHeadsetStateChanged()
        case .categoryChange:
            print("categoryChange")
            onHeadsetStateChanged()
        default:
            print("not handling reason")
        }
    }
    
    func onHeadsetStateChanged() {
        for soundPlayer in soundsPlayers {
            soundPlayer.setupVolume(headSetPluggedIn: RehegooConfig.headsetPluggedIn())
        }
    }
    
    @objc func onSoundVolumeChanged(notification: Notification) {
        let soundIndex : Int = notification.object as! Int
        
        soundsPlayers[soundIndex - 1].setupVolume(headSetPluggedIn: RehegooConfig.headsetPluggedIn())
    }
    
    @objc func onSoundSelectedStateChanged(notification: Notification) {
        let soundIndex : Int = notification.object as! Int
        
        soundsPlayers[soundIndex - 1].pause()
    }
    
    func initSoundsControllers() {
        for i in 1...SoundsRepository.shared.getNumberOfSounds() {
            soundsPlayers.append(SoundPlayer(soundIndex: i))
        }
    }
    
    func onPlayTapped(soundIndex: Int) {
        soundsPlayers[soundIndex - 1].isPlaying() ?
            soundsPlayers[soundIndex - 1].pause() :
            soundsPlayers[soundIndex - 1].play()
    }
    
    func stopAllSounds() {
        for soundPlayer in soundsPlayers {
            soundPlayer.pause()
        }
    }
    
    func isSoundPlaying(soundIndex: Int) -> Bool {
        return soundsPlayers[soundIndex - 1].isPlaying()
    }
}
