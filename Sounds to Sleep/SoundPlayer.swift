//
//  SoundController.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 16/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import AVFoundation

enum Pan: Float {
    case Left = -1.0, Right = 1.0
}

class SoundPlayer: NSObject {
    let shortStartDelay = 0.01
    var soundPlayerIndex: Int
    var leftSoundPlayer: AVAudioPlayer!
    var rightSoundPlayer: AVAudioPlayer!

    required init(soundIndex: Int) {
        soundPlayerIndex = soundIndex

        super.init()

        initSoundPlayers()
    }

    func initSoundPlayers() {
        leftSoundPlayer = setupPlayer (withPan: Pan.Left.rawValue)
        rightSoundPlayer = setupPlayer(withPan: Pan.Right.rawValue)

        setupVolume(headSetPluggedIn: RehegooConfig.headsetPluggedIn())
    }

    func setupPlayer(withPan pan: Float) -> AVAudioPlayer {
        let player: AVAudioPlayer
        print("loading sound file with soundIndex:\(soundPlayerIndex)")
        let path = Bundle.main.path(forResource: "sound\(soundPlayerIndex)", ofType: ".wav")!
        let url = URL(fileURLWithPath: path)

        do {
            player = try AVAudioPlayer(contentsOf: url)
            player.numberOfLoops = -1
            player.pan = pan
            player.prepareToPlay()
            return player;
        } catch {
            print("couldn't load sound file with soundIndex:\(soundPlayerIndex)")
        }

        return AVAudioPlayer()
    }

    func setupVolume(headSetPluggedIn: Bool) {
        var leftPlayerVolume: Float = 1
        var rightPlayerVolume: Float = 1

        let volumeSoundIndex = SoundsRepository.shared.getSound(soundIndex: soundPlayerIndex)?.index

        if(headSetPluggedIn) {
            leftPlayerVolume = RehegooConfig.getSoundLeftPanValue (soundIndex: volumeSoundIndex!)
            rightPlayerVolume = RehegooConfig.getSoundRightPanValue (soundIndex: volumeSoundIndex!)
        }

        let soundVolume: Float = RehegooConfig.getSoundVolume(soundIndex: volumeSoundIndex!)

        leftSoundPlayer.volume = leftPlayerVolume * soundVolume
        rightSoundPlayer.volume = rightPlayerVolume * soundVolume
    }

    func play() {
        let now = leftSoundPlayer.deviceCurrentTime;

        leftSoundPlayer.play(atTime: now + shortStartDelay)
        rightSoundPlayer.play(atTime: now + shortStartDelay)
    }

    func pause() {
        leftSoundPlayer.pause()
        rightSoundPlayer.pause()
    }

    func isPlaying() -> Bool {
        return leftSoundPlayer.isPlaying
    }
}
