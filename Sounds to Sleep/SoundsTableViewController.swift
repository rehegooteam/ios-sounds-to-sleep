//
//  SoundsTableViewController.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 07/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import AVFoundation

class SoundsTableViewController: UITableViewController, SoundCellDelegate {
    
    private var selectedSoundsIndexes : [Int] = RehegooConfig.getSelectedSoundsIndexes()
    private var settingsSoundCellIndex : Int = -1
    
    override func awakeFromNib() {
        NotificationCenter.default.addObserver(self, selector: #selector(onSoundSelectedStateChanged(notification:)), name: NSNotification.Name(rawValue: RehegooConfig.SELECTED_SOUND_KEY), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onPremiumPurchased), name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onTimerEnded), name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_ENDED), object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(handleRouteChange(notification:)),
                                               name: AVAudioSession.routeChangeNotification,
                                               object: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @objc func handleRouteChange(notification: Notification) {
        guard
            let userInfo = notification.userInfo,
            let reasonRaw = userInfo[AVAudioSessionRouteChangeReasonKey] as? NSNumber,
            let reason = AVAudioSession.RouteChangeReason(rawValue: reasonRaw.uintValue)
            else { fatalError("Strange... could not get routeChange") }
        switch reason {
        case .oldDeviceUnavailable:
            print("oldDeviceUnavailable")
            onHeadsetStateChanged()
        case .newDeviceAvailable:
            print("newDeviceAvailable")
            onHeadsetStateChanged()
        case .routeConfigurationChange:
            print("routeConfigurationChange")
            onHeadsetStateChanged()
        case .categoryChange:
            print("categoryChange")
            onHeadsetStateChanged()
        default:
            print("not handling reason")
        }
    }

    func onHeadsetStateChanged() {
        
        var indexPaths : [IndexPath] = Array()
        
        for i in 1...tableView.numberOfRows(inSection: 0) - 1 {
            
            if(i % 2 != 0) {
                indexPaths.append(IndexPath(row: i, section: 0))
            }
        }
        
        DispatchQueue.main.async {
            self.tableView.reloadRows(at: indexPaths, with: .automatic)
        }
    }
    
    @objc func onTimerEnded() {
        tableView.reloadSections([0], with: .automatic)
    }
    
    @objc func onPremiumPurchased() {
        tableView.reloadSections([0], with: .automatic)
    }
    
    @objc func onSoundSelectedStateChanged(notification: Notification) {
        //let songIndex : Int = notification.object as! Int
        
        selectedSoundsIndexes = RehegooConfig.getSelectedSoundsIndexes()
        
        settingsSoundCellIndex = -1
        
        tableView.reloadSections([0], with: .automatic)
    }
    
    func getSoundCellIndex(from indexPath : IndexPath) -> Int {
        assert(indexPath.row % 2 == 0, "Wrong Sound Index: \(indexPath.row)")
        
        return Int(ceil(CGFloat(indexPath.row) / 2.0))
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return selectedSoundsIndexes.count * 2
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cellHeight : CGFloat
        
        if(indexPath.row % 2 != 0) {
            if(settingsSoundCellIndex == indexPath.row) {
                cellHeight = SettingsSoundTableViewCell.CELL_HEIGHT
            } else {
                cellHeight = 0
            }
        } else {
            cellHeight = SoundTableViewCell.CELL_HEIGHT
        }
        
        return cellHeight
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.row % 2 != 0) {
            let cell : SettingsSoundTableViewCell = tableView.dequeueReusableCell(withIdentifier: SettingsSoundTableViewCell.CELL_ID, for: indexPath) as! SettingsSoundTableViewCell
            
            let sound = SoundsRepository.shared.getSound(soundIndex: selectedSoundsIndexes[getSoundCellIndex(from: IndexPath(row: indexPath.row - 1, section: 0))])
                
            cell.setupView(with: sound!)
            
            return cell
        }
        
        let cell : SoundTableViewCell = tableView.dequeueReusableCell(withIdentifier: SoundTableViewCell.CELL_ID, for: indexPath) as! SoundTableViewCell
        cell.soundCellDelegate = self;
        let sound = SoundsRepository.shared.getSound(soundIndex: selectedSoundsIndexes[getSoundCellIndex(from: indexPath)])
        
        cell.setupView(with: sound!, and: indexPath.row, settingsOpened: settingsSoundCellIndex == indexPath.row + 1 ? true : false)
        
        return cell
    }

    // MARK: - SoundCellDelegate Methods
    func onSettingsTapped(cellIndex: Int, soundIndex: Int) -> Bool {
        RehegooGADInterstitialController.shared.presentInterstitial(fromRootViewController: self, withProbability: 30)
        
        let newSettingsSoundCellIndex = cellIndex + 1
        let oldSettingsSoundCellIndex = settingsSoundCellIndex
    
        settingsSoundCellIndex = newSettingsSoundCellIndex == oldSettingsSoundCellIndex ? -1 : newSettingsSoundCellIndex
        
        //tableView.reloadRows(at: [IndexPath(row: newSettingsSoundCellIndex, section: 0)], with: .automatic) // Reload Sound Setting Cell
        
        if (newSettingsSoundCellIndex != oldSettingsSoundCellIndex) {
            tableView.reloadRows(at: [IndexPath(row: oldSettingsSoundCellIndex - 1, section: 0),
                                      IndexPath(row: newSettingsSoundCellIndex, section: 0)],
                                 with: .automatic) // Reload Old
        } else {
            tableView.reloadRows(at: [IndexPath(row: newSettingsSoundCellIndex, section: 0)], with: .automatic)
        }
        
        return settingsSoundCellIndex != -1 ? true : false
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
