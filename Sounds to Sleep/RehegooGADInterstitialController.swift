//
//  RehegooGADInterstitial.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import GoogleMobileAds

class RehegooGADInterstitialController: NSObject, GADInterstitialDelegate {
    static let shared : RehegooGADInterstitialController = RehegooGADInterstitialController()
    
    var interstitial : GADInterstitial? = GADInterstitial(adUnitID: RehegooConstants.GAD_INTERSTITIAL_ID)
    
    func loadInterstitial() {
        guard !RehegooConfig.isPremium() else {
            interstitial = nil
            print("interstitial can not be processed! (Premium Purchased)")
            return;
        }
        
        interstitial = GADInterstitial(adUnitID: RehegooConstants.GAD_INTERSTITIAL_ID)
        interstitial?.delegate = self;
        
        let request = GADRequest()
        
        request.testDevices = RehegooConstants.TEST_DEVICES
        
        interstitial?.load(request)
    }

    /**
     1. Probability 1 - 99 (%)
     */
    func presentInterstitial(fromRootViewController rootViewController: UIViewController, withProbability probability: Int) {
        guard !RehegooConfig.isPremium() else {
            print("interstitial can not be processed! (Premium Purchased)")
            return;
        }
        
        let random = (1...99).randomInt
        
        if probability > random {
            presentInterstitial(fromRootViewController: rootViewController)
        }
    }
    
    func presentInterstitial(fromRootViewController rootViewController: UIViewController) {
        guard !RehegooConfig.isPremium() else {
            print("interstitial can not be processed! (Premium Purchased)")
            return;
        }
        
        if (interstitial?.isReady)! {
            interstitial?.present(fromRootViewController: rootViewController)
        } else if (interstitial?.hasBeenUsed)! {
            loadInterstitial()
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        loadInterstitial()
    }
}
