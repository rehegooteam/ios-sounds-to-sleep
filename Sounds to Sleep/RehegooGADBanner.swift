//
//  RehegooGADBanner.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import GoogleMobileAds

class RehegooGADBanner: GADBannerView {
    
    var GADBannerHeight : CGFloat;
    var GADBannerWidth : CGFloat;
    
    required init?(coder aDecoder: NSCoder) {
        
        let bannerView = GADBannerView(adSize: kGADAdSizeSmartBannerPortrait)
        GADBannerHeight = bannerView.frame.size.height
        GADBannerWidth = bannerView.frame.size.width
        
        super.init(coder: aDecoder)
        
        DispatchQueue.once(token: "configure_gadmobileads_once")  {
            GADMobileAds.configure(withApplicationID: RehegooConstants.AD_MOB_PUBLISHER_ID)
            print("Google Mobile Ads SDK version: \(GADRequest.sdkVersion())")
        }
    }
    
    func initialize(vc: UIViewController) {
        guard !RehegooConfig.isPremium() else {
            print("adBanner can not be processed! (Premium Purchased)")
            return;
        }
        
        self.adUnitID = RehegooConstants.GAD_BANNER_ID
        self.rootViewController = vc
        
        let gadRequest = GADRequest()
        
        gadRequest.testDevices = RehegooConstants.TEST_DEVICES
        
        self.load(gadRequest)
    }
}


