//
//  TimerButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 20/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class TimerButton: UIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addObservers()
        setRoundedCorners()
        setTextAndColor()
        setupView()
        
        self.addTarget(self, action: #selector(onTouched), for: .touchDown)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(setTextAndColor),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_STATE_CHANGED),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(setupView),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_TIME_CHANGED),
            object: nil)
    }
    
    @objc func setTextAndColor() {
        let isTimerOn = TimerManager.shared.isOn
        
        self.setTitle(NSLocalizedString(isTimerOn ? "stop_timer" : "start_timer", comment: ""), for: .normal)
        
        self.backgroundColor = isTimerOn ? UIColor.init(hexString: "#F44336") : UIColor.init(hexString: "#8BC34A")
    }
    
    @objc func setupView() {
        RehegooConfig.getTotalSeconds() == 0 ? disableView() : enableView()
    }
    
    func disableView() {
        self.isEnabled = false
        self.alpha = 0.75
    }
    
    func enableView() {
        self.isEnabled = true
        self.alpha = 1
    }
    
    func setRoundedCorners() {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    @objc func onTouched() {
        Animate.bounce(view: self, type: .Light);
        
        TimerManager.shared.onStartTapped()
    }
}
