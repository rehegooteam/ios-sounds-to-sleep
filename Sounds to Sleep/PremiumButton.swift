//
//  PremiumButton.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 08/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class PremiumButton: LeftImageUIButton {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addObservers()
        setupTitle()
        setupView()
        setupImage()
        
        self.addTarget(self, action: #selector(onTouched), for: .touchDown)
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onPremiumPurchased),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED),
            object: nil)
    }
    
    @objc func onPremiumPurchased() {
        setupTitle()
        setupImage()
    }
    
    func setupImage() {
        RehegooConfig.isPremium() ? setButtonImage(imageName: "add") : setButtonImage(imageName: "diamond")
    }
    
    func setupTitle() {
        var title : String
        
        title = NSLocalizedString("add_sound", comment: "")
        
        self.setTitle(title, for: .normal)
    }
    
    func setupView() {
        self.layer.cornerRadius = self.frame.height / 2
    }
    
    @objc func onTouched() {
        Animate.bounce(view: self, type: .Light);
    }
}
