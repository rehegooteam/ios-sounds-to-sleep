//
//  RehegooConfig.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import AVFoundation

class RehegooConfig: NSObject {

    static let PREMIUM_KEY = "ispremiumrehegoo"
    static let SELECTED_SOUND_KEY = "selectedsound"
    static let SELECTED_SOUNDS_INDEXES_KEY = "numberofselectedsounds"
    static let FIRST_APP_LAUNCH_KEY = "applaunchedbefore"
    static let FIRST_APP_LAUNCH_KEY_VERSION2 = "applaunchedbefore"
    static let SOUND_VOLUME = "soundvolume"
    static let SOUND_LEFT_PAN = "soundleftpan"
    static let SOUND_RIGHT_PAN = "soundrightpan"
    static let HOURS_KEY = "hourskey"
    static let MINUTES_KEY = "minuteskey"
    static let SECONDS_KEY = "secondskey"
    static let FREE_SOUND_OF_THE_DAY_KEY = "freesoundofthedayindexkey"
    static let FREE_SOUND_DAY_KEY = "freesounddaykey"
    static let BRIGHTNESS_LEVEL_KEY = "brightnesslevelkey"
    
    static let userDefaults = Foundation.UserDefaults.standard

    public static func registerDefaults() {
        if(!userDefaults.bool(forKey: FIRST_APP_LAUNCH_KEY_VERSION2)) {
            setBrightnessLevel(brightnessLevel: 0.5)
            changeFreeSoundOfTheDayIndex()
            
            setFreeSoundDay(day: getCurrentDay())
        } else {
            userDefaults.set(true, forKey: FIRST_APP_LAUNCH_KEY_VERSION2)
        }
        
        if(userDefaults.bool(forKey: FIRST_APP_LAUNCH_KEY)) {
            return
        } else {
            userDefaults.set(true, forKey: FIRST_APP_LAUNCH_KEY)
        }

        for i in 1...SoundsRepository.shared.getNumberOfSounds() {
            sound(isSelected: i > RehegooConstants.NUMBER_OF_FREE_SOUNDS ? false : true, soundIndex: i)
            setSoundVolume(soundIndex: i, volume: 0.6)
            setSoundLeftPanValue(soundIndex: i, value: 1.0)
            setSoundRightPanValue(soundIndex: i, value: 1.0)
        }
    }

    public static func getCurrentDay() -> Int {
        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.day], from: date)
        let day = components.day

        return day!
    }

    public static func headsetPluggedIn() -> Bool {
        let route = AVAudioSession.sharedInstance().currentRoute
        return (route.outputs).filter({ convertFromAVAudioSessionPort($0.portType) == convertFromAVAudioSessionPort(AVAudioSession.Port.headphones) }).count > 0
    }

    public static func isPremium() -> Bool {
        //return true
        return userDefaults.bool(forKey: PREMIUM_KEY)
    }

    public static func onPremiumPurchased() {
        userDefaults.set(true, forKey: PREMIUM_KEY)

        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_PREMIUM_PURCHASED), object: nil)
    }
    
    public static func setBrightnessLevel(brightnessLevel: Float) {
        userDefaults.set(brightnessLevel, forKey: BRIGHTNESS_LEVEL_KEY)
    }
    
    public static func getBrightnessLevel() -> Float {
        
        var brightnessLevel = userDefaults.float(forKey: BRIGHTNESS_LEVEL_KEY)
        
        if(brightnessLevel == 0.0) {
            brightnessLevel = 0.5
        }
        
        return brightnessLevel
    }

    public static func getFreeSoundDay() -> Int {
        return userDefaults.integer(forKey: FREE_SOUND_DAY_KEY)
    }

    public static func setFreeSoundDay(day: Int) {
        userDefaults.set(day, forKey: FREE_SOUND_DAY_KEY)
    }

    public static func getFreeSoundOfTheDayIndex() -> Int {
        return userDefaults.integer(forKey: FREE_SOUND_OF_THE_DAY_KEY)
    }

    public static func changeFreeSoundOfTheDayIndex() {
        let currentFreeSoundOfTheDayIndex = getFreeSoundOfTheDayIndex()
        var newFreeSoundOfTheDayIndex = currentFreeSoundOfTheDayIndex

        repeat {
            newFreeSoundOfTheDayIndex = (RehegooConstants.NUMBER_OF_FREE_SOUNDS + 1 ... SoundsRepository.shared.sounds.count).randomInt
        } while (currentFreeSoundOfTheDayIndex == newFreeSoundOfTheDayIndex)

        userDefaults.set(newFreeSoundOfTheDayIndex, forKey: FREE_SOUND_OF_THE_DAY_KEY)
    }

    public static func getTotalSeconds() -> Int {
        return getHours() * 3600 + getMinutes() * 60 + getSeconds()
    }

    public static func getHours() -> Int {
        return userDefaults.integer(forKey: HOURS_KEY)
    }

    public static func setHours(value: Int) {
        userDefaults.set(value, forKey: HOURS_KEY)
        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIME_CHANGED), object: nil)
    }

    public static func getMinutes() -> Int {
        return userDefaults.integer(forKey: MINUTES_KEY)
    }

    public static func setMinutes(value: Int) {
        userDefaults.set(value, forKey: MINUTES_KEY)
        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIME_CHANGED), object: nil)
    }

    public static func getSeconds() -> Int {
        return userDefaults.integer(forKey: SECONDS_KEY)
    }

    public static func setSeoncds(value: Int) {
        userDefaults.set(value, forKey: SECONDS_KEY)
        NotificationCenter.default.post(name: Notification.Name(RehegooConstants.ON_TIME_CHANGED), object: nil)
    }

    public static func getSoundVolume(soundIndex: Int) -> Float {
        return userDefaults.float(forKey: SOUND_VOLUME + soundIndex.description)
    }

    public static func setSoundVolume(soundIndex: Int, volume: Float) {
        userDefaults.set(volume, forKey: SOUND_VOLUME + soundIndex.description)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RehegooConstants.ON_VOLUME_CHANGED), object: soundIndex)
    }

    public static func getSoundLeftPanValue(soundIndex: Int) -> Float {
        return userDefaults.float(forKey: SOUND_LEFT_PAN + soundIndex.description)
    }

    public static func setSoundLeftPanValue(soundIndex: Int, value: Float) {
        userDefaults.set(value, forKey: SOUND_LEFT_PAN + soundIndex.description)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RehegooConstants.ON_VOLUME_CHANGED), object: soundIndex)
    }

    public static func getSoundRightPanValue(soundIndex: Int) -> Float {
        return userDefaults.float(forKey: SOUND_RIGHT_PAN + soundIndex.description)
    }

    public static func setSoundRightPanValue(soundIndex: Int, value: Float) {
        userDefaults.set(value, forKey: SOUND_RIGHT_PAN + soundIndex.description)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: RehegooConstants.ON_VOLUME_CHANGED), object: soundIndex)
    }

    public static func isSoundSelected(soundIndex: Int) -> Bool {
        return userDefaults.bool(forKey: SELECTED_SOUND_KEY + soundIndex.description)
    }

    public static func sound(isSelected: Bool, soundIndex: Int) {
        userDefaults.set(isSelected, forKey: SELECTED_SOUND_KEY + soundIndex.description)

        var selectedSoundsArray = getSelectedSoundsIndexes()
        if(isSelected) {
            selectedSoundsArray.append(soundIndex)
        } else {
            if(selectedSoundsArray.contains(soundIndex)) {
                selectedSoundsArray.remove(at: selectedSoundsArray.firstIndex(of: soundIndex)!)
            }
        }

        userDefaults.set(NSKeyedArchiver.archivedData(withRootObject: selectedSoundsArray), forKey: SELECTED_SOUNDS_INDEXES_KEY)

        NotificationCenter.default.post(name: NSNotification.Name(rawValue: SELECTED_SOUND_KEY), object: soundIndex)
    }

    public static func getSelectedSoundsIndexes() -> [Int] {
        if (isKeyPresentInUserDefaults(key: SELECTED_SOUNDS_INDEXES_KEY)) {

            let selectedSoundsIndexesData: Data = userDefaults.data(forKey: SELECTED_SOUNDS_INDEXES_KEY)!;

            return NSKeyedUnarchiver.unarchiveObject(with: selectedSoundsIndexesData) as! [Int]
        } else {
            return Array()
        }
    }

    public static func isKeyPresentInUserDefaults(key: String) -> Bool {
        return UserDefaults.standard.object(forKey: key) != nil
    }
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionPort(_ input: AVAudioSession.Port) -> String {
	return input.rawValue
}
