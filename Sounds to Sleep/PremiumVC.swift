//
//  PremiumVC.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit
import StoreKit
import SwiftyStoreKit
import Auk

class PremiumVC: UIViewController {
    @IBOutlet weak var purchaseButton: PurchaseButton!
    @IBOutlet weak var restorePurchaseBtn: UIButton!
    @IBOutlet weak var purchaseInd: UIActivityIndicatorView!
    @IBOutlet weak var restorePurchaseInd: UIActivityIndicatorView!
    @IBOutlet weak var premiumFeaturesScrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        loadPremium()
        setupPremiumFeatures()
    }

    func setupPremiumFeatures() {
        
        for i in 1...3 {
            if let image = UIImage(named: "premium_feature\(i)") {
                premiumFeaturesScrollView.auk.show(image: image)
            }
        }
        
        premiumFeaturesScrollView.auk.startAutoScroll(delaySeconds: 1.5)
    }

    func loadPremium() {
        purchaseInd.startAnimating()
        UIApplication.shared.isNetworkActivityIndicatorVisible = true

        SwiftyStoreKit.retrieveProductsInfo([RehegooConstants.PREMIUM_PRODUCT_ID]) { result in
            if let product = result.retrievedProducts.first {
                let priceString = product.localizedPrice!

                print("Product: \(product.localizedDescription), price: \(priceString)")

                self.purchaseButton.setTitle(NSLocalizedString("premium", comment: "") + " " + priceString, for: .normal)

                self.purchaseInd.stopAnimating()
            }
                else if result.invalidProductIDs.first != nil {
                //return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
            }
                else {
                print("Error: \(String(describing: result.error))")
            }

            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }

    @IBAction func onPurchaseTapped(_ sender: Any) {
        disableView(ind: purchaseInd)

        SwiftyStoreKit.purchaseProduct(RehegooConstants.PREMIUM_PRODUCT_ID, atomically: true) { result in
            switch result {
            case .success(let product):
                print("Purchase Success: \(product.productId)")
                self.onPremiumPurchased()
            case .error(let error):
                print("Purchase Failed: \(error)")
            }

            self.enableView(ind: self.purchaseInd)
        }
    }

    @IBAction func onRestorePurchaseTapped(_ sender: Any) {
        disableView(ind: restorePurchaseInd)

        SwiftyStoreKit.restorePurchases(atomically: true) { results in

            if results.restoreFailedPurchases.count > 0 {
                self.showAlert(self.alertWithTitle(
                NSLocalizedString("Attention!", comment: ""),
                                                   message: results.restoreFailedPurchases[0].0.localizedDescription))
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
                else if results.restoredPurchases.count > 0 {
                print("Restore Success: \(results.restoredPurchases)")
                self.onPremiumPurchased()
            }
                else {
                self.showAlert(self.alertWithTitle(
                NSLocalizedString("Attention!", comment: ""),
                                                   message: NSLocalizedString("no_purchases_to_restore", comment: "")))
                print("Nothing to Restore")
            }

            self.enableView(ind: self.restorePurchaseInd)
        }
    }

    func disableView(ind: UIActivityIndicatorView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        self.view.isUserInteractionEnabled = false
        self.view.alpha = 0.5
        ind.startAnimating()
    }

    func enableView(ind: UIActivityIndicatorView) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        self.view.isUserInteractionEnabled = true
        self.view.alpha = 1
        ind.stopAnimating()
    }

    func onPremiumPurchased() {
        RehegooConfig.onPremiumPurchased()

        _ = navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//
//// MARK: User facing alerts
//extension ViewController {
//
//    func alertForProductRetrievalInfo(_ result: RetrieveResults) -> UIAlertController {
//
//        if let product = result.retrievedProducts.first {
//            let priceString = product.localizedPrice!
//            return alertWithTitle(product.localizedTitle, message: "\(product.localizedDescription) - \(priceString)")
//        }
//        else if let invalidProductId = result.invalidProductIDs.first {
//            return alertWithTitle("Could not retrieve product info", message: "Invalid product identifier: \(invalidProductId)")
//        }
//        else {
//            let errorString = result.error?.localizedDescription ?? "Unknown error. Please contact support"
//            return alertWithTitle("Could not retrieve product info", message: errorString)
//        }
//    }
//
//    func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController {
//        switch result {
//        case .success(let product):
//            print("Purchase Success: \(product.productId)")
//            return alertWithTitle("Thank You", message: "Purchase completed")
//        case .error(let error):
//            print("Purchase Failed: \(error)")
//            switch error {
//            case .failed(let error):
//                if (error as NSError).domain == SKErrorDomain {
//                    return alertWithTitle("Purchase failed", message: "Please check your Internet connection or try again later")
//                }
//                return alertWithTitle("Purchase failed", message: "Unknown error. Please contact support")
//            case .invalidProductId(let productId):
//                return alertWithTitle("Purchase failed", message: "\(productId) is not a valid product identifier")
//            case .paymentNotAllowed:
//                return alertWithTitle("Payments not enabled", message: "You are not allowed to make payments")
//            }
//        }
//    }
//
//    func alertForRestorePurchases(_ results: RestoreResults) -> UIAlertController {
//
//        if results.restoreFailedProducts.count > 0 {
//            print("Restore Failed: \(results.restoreFailedProducts)")
//            return alertWithTitle("Restore failed", message: "Unknown error. Please contact support")
//        }
//        else if results.restoredProducts.count > 0 {
//            print("Restore Success: \(results.restoredProducts)")
//            return alertWithTitle("Purchases Restored", message: "All purchases have been restored")
//        }
//        else {
//            print("Nothing to Restore")
//            return alertWithTitle("Nothing to restore", message: "No previous purchases were found")
//        }
//    }
//
//
//    func alertForVerifyReceipt(_ result: VerifyReceiptResult) -> UIAlertController {
//
//        switch result {
//        case .success(let receipt):
//            print("Verify receipt Success: \(receipt)")
//            return alertWithTitle("Receipt verified", message: "Receipt verified remotly")
//        case .error(let error):
//            print("Verify receipt Failed: \(error)")
//            switch (error) {
//            case .noReceiptData :
//                return alertWithTitle("Receipt verification", message: "No receipt data, application will try to get a new one. Try again.")
//            default:
//                return alertWithTitle("Receipt verification", message: "Receipt verification failed")
//            }
//        }
//    }
//
//    func alertForVerifySubscription(_ result: VerifySubscriptionResult) -> UIAlertController {
//
//        switch result {
//        case .purchased(let expiresDate):
//            print("Product is valid until \(expiresDate)")
//            return alertWithTitle("Product is purchased", message: "Product is valid until \(expiresDate)")
//        case .expired(let expiresDate):
//            print("Product is expired since \(expiresDate)")
//            return alertWithTitle("Product expired", message: "Product is expired since \(expiresDate)")
//        case .notPurchased:
//            print("This product has never been purchased")
//            return alertWithTitle("Not purchased", message: "This product has never been purchased")
//        }
//    }
//
//    func alertForVerifyPurchase(_ result: VerifyPurchaseResult) -> UIAlertController {
//
//        switch result {
//        case .purchased:
//            print("Product is purchased")
//            return alertWithTitle("Product is purchased", message: "Product will not expire")
//        case .notPurchased:
//            print("This product has never been purchased")
//            return alertWithTitle("Not purchased", message: "This product has never been purchased")
//        }
//    }
//
//    func alertForRefreshReceipt(_ result: RefreshReceiptResult) -> UIAlertController {
//        switch result {
//        case .success(let receiptData):
//            print("Receipt refresh Success: \(receiptData.base64EncodedString)")
//            return alertWithTitle("Receipt refreshed", message: "Receipt refreshed successfully")
//        case .error(let error):
//            print("Receipt refresh Failed: \(error)")
//            return alertWithTitle("Receipt refresh failed", message: "Receipt refresh failed")
//        }
//    }

//}
