//
//  NavigationController.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 09/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class NavigationController: NSObject {
    public static let shared: NavigationController = NavigationController()
    
    private var premiumVC: PremiumVC!
    private var soundsPickerVC: SoundsPickerVC!
    
    func initialize(withStoryboard storyboard: UIStoryboard) {
        if(!RehegooConfig.isPremium()) {
            premiumVC = (storyboard.instantiateViewController(withIdentifier: "premiumVC") as! PremiumVC)
        }
        soundsPickerVC = (storyboard.instantiateViewController(withIdentifier: "soundsPickerVC") as! SoundsPickerVC)
    }
    
    func navigateToPurchase(fromVC vc: UIViewController) {
        assert(!RehegooConfig.isPremium(), "Can not proceed to PurchaseVC because Premium is PURCHASED!")
        
        guard !(vc.navigationController?.topViewController?.isKind(of: PremiumVC.self))! else {
            return
        }
        
        vc.navigationController?.show(premiumVC, sender: self)
    }
    
    func navigateToSoundsPicker(fromVC vc: UIViewController) {
        
        guard !(vc.navigationController?.topViewController?.isKind(of: SoundsPickerVC.self))! else {
            return
        }
        
        vc.navigationController?.show(soundsPickerVC, sender: self)
    }
}
