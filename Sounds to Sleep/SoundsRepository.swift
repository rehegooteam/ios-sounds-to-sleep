//
//  SoundsRepository.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 08/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class SoundsRepository: NSObject {
    
    static let shared : SoundsRepository = SoundsRepository()

    var sounds : Array<Sound> = Array()
    
    required override init() {
        super.init()
        
        initRepository()
    }
    
    func getNumberOfSounds() -> NSInteger {
        return sounds.count
    }
    
    func initRepository() -> Void {
//        for i in 1...RehegooConstants.NUMBER_OF_SOUNDS {
//            sounds.append(Sound(soundIndex: i))
//        }
        
        var soundsGroupName = "noise"
        var soundsGroup : Array<Sound> = Array()
        
        soundsGroup.append(Sound(soundIndex: 1, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 2, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 3, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "ambience"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 4, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 5, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 6, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 7, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 8, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "fan"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 9, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 10, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 11, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "fire_crackling"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 12, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "nature"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 13, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 14, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 40, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "rain"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 15, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 16, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 17, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 18, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 27, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "river"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 19, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 20, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "sea"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 21, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 22, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 28, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 33, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 34, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 35, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 36, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 37, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "under_water"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 23, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "vacuum"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 24, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 25, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "water_dripping"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 26, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "insect"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 29, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 43, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 44, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 45, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 46, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "bird"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 30, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 31, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 32, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 41, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "boat"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 38, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 39, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 42, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "wind"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 47, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 48, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 49, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 50, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 51, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
        soundsGroupName = "asmr"
        soundsGroup = Array()
        
        soundsGroup.append(Sound(soundIndex: 52, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 53, soundIconName: soundsGroupName))
        soundsGroup.append(Sound(soundIndex: 54, soundIconName: soundsGroupName))
        
        sounds.append(contentsOf: soundsGroup)
        
//        sounds[0].iconName = "noise"
//        sounds[1].iconName = "noise"
//        sounds[2].iconName = "noise"
//        sounds[3].iconName = "ambience"
//        sounds[4].iconName = "ambience"
//        sounds[5].iconName = "ambience"
//        sounds[6].iconName = "ambience"
//        sounds[7].iconName = "ambience"
//        sounds[8].iconName = "fan"
//        sounds[9].iconName = "fan"
//        sounds[10].iconName = "fan"
//        sounds[11].iconName = "fire_crackling"
//        sounds[12].iconName = "nature"
//        sounds[13].iconName = "nature"
//        sounds[14].iconName = "rain"
//        sounds[15].iconName = "rain"
//        sounds[16].iconName = "rain"
//        sounds[17].iconName = "rain"
//        sounds[18].iconName = "river"
//        sounds[19].iconName = "river"
//        sounds[20].iconName = "sea"
//        sounds[21].iconName = "sea"
//        sounds[22].iconName = "under_water"
//        sounds[23].iconName = "vacuum"
//        sounds[24].iconName = "vacuum"
//        sounds[25].iconName = "water_dripping"
    }
    
    func getSound(soundIndex: Int) -> Sound? {
        
        for sound in sounds {
            if(sound.index == soundIndex) {
                return sound
            }
        }
        
        return nil
        //return sounds[soundIndex - 1]
    }
}
