//
//  FreeSoundScene.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 04/07/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

class FreeSoundScene: UIViewController {
    
    @IBOutlet weak var playBtn: UIButton!
    
    @IBOutlet weak var soundBackgroundView: UIView!
    @IBOutlet weak var soundImageView: UIImageView!
    @IBOutlet weak var soundNameLabel: UILabel!
    
    var currentSoundIndex = RehegooConfig.getFreeSoundOfTheDayIndex()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(changeFreeSoundOfTheDayIfNeeded),
                                               name: UIApplication.willEnterForegroundNotification,
                                               object: nil)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onPremiumPurchased),
                                               name: NSNotification.Name(rawValue: RehegooConstants.ON_PREMIUM_PURCHASED),
                                               object: nil)
    }
    
    @IBAction func onFreeSoundViewTapped(_ sender: Any) {
        NavigationController.shared.navigateToPurchase(fromVC: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (RehegooConfig.isPremium()) {
            return
        }
        
        setupSoundBackgroundView()
        setupSound()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        changeFreeSoundOfTheDayIfNeeded()
    }
    
    @objc func onPremiumPurchased() {
        
        SoundsManager.shared.stopAllSounds()
    }
    
    @objc func changeFreeSoundOfTheDayIfNeeded() {
        let currentDay = RehegooConfig.getCurrentDay()
        
        if(currentDay != RehegooConfig.getFreeSoundDay()) {
            RehegooConfig.changeFreeSoundOfTheDayIndex()
            currentSoundIndex = RehegooConfig.getFreeSoundOfTheDayIndex()
            RehegooConfig.setFreeSoundDay(day: currentDay)
        }
    }
    
    func setupSoundBackgroundView() {
        soundBackgroundView.layer.cornerRadius = 27
        soundBackgroundView.layer.borderWidth = 1;
        soundBackgroundView.layer.borderColor = UIColor.white.cgColor
        soundBackgroundView.backgroundColor = UIColor.clear
    }
    
    func setupSound() {
        let sound = SoundsRepository.shared.getSound(soundIndex: currentSoundIndex)
        
        soundNameLabel.text = sound?.name
        soundImageView.image = UIImage.init(named: (sound?.iconName)!)
    }
    
    @IBAction func onPlayTapped(_ sender: Any) {
        Animate.bounce(view: sender as! UIView, type: .Light)
        
        SoundsManager.shared.onPlayTapped(soundIndex: currentSoundIndex)
        
        onSoundStatusChanged()
    }
    
    func onSoundStatusChanged() {
        
        let isSoundPlaying : Bool = SoundsManager.shared.isSoundPlaying(soundIndex: currentSoundIndex)
        
        playBtn.setImage(isSoundPlaying ? SoundTableViewCell.pauseImage : SoundTableViewCell.playImage, for: .normal)
        
        soundBackgroundView.backgroundColor = isSoundPlaying ? SoundTableViewCell.playBackgroundColor : UIColor.clear
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
