//
//  HoursPickerView.swift
//  Sounds to Sleep
//
//  Created by Lucas Bartoszewski on 20/02/2017.
//  Copyright © 2017 Rehegoo. All rights reserved.
//

import UIKit

enum TimePickerType {
    case Hours
    case Minutes
    case Seconds
}

class TimePickerView: UIPickerView, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var numberOfRows : Int?
    var pickerType : TimePickerType?
    let rowHeight = 40

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        addObservers()
        
        self.delegate = self
        self.dataSource = self
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onTimerStateChanged),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_STATE_CHANGED),
            object: nil)
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(onTimerSecondsChanged(notification:)),
            name: NSNotification.Name(rawValue: RehegooConstants.ON_TIMER_SECONDS_CHANGED),
            object: nil)
    }
    
    @objc func onTimerSecondsChanged(notification: Notification) {
        let timerSeconds : Int = notification.object as! Int
        
        UIView.animate(withDuration: 0.1, animations: {
            switch self.pickerType! {
            case TimePickerType.Hours:
                self.selectRow(timerSeconds.toHours(), inComponent: 0, animated: false)
            case TimePickerType.Minutes:
                self.selectRow(timerSeconds.toMinutes(), inComponent: 0, animated: false)
            case TimePickerType.Seconds:
                self.selectRow(timerSeconds.toSeconds(), inComponent: 0, animated: false)
            }
        })
    }
    
    @objc func onTimerStateChanged() {
        isUserInteractionEnabled = TimerManager.shared.isOn ? false : true
        setInitialValue(animate: true)
    }
    
    func setInitialValue(animate: Bool) {
        switch pickerType! {
        case TimePickerType.Hours:
            selectRow(RehegooConfig.getHours(), inComponent: 0, animated: animate)
        case TimePickerType.Minutes:
            selectRow(RehegooConfig.getMinutes(), inComponent: 0, animated: animate)
        case TimePickerType.Seconds:
            selectRow(RehegooConfig.getSeconds(), inComponent: 0, animated: animate)
        }
    }
    
    // MARK - UIPickerViewDataSource
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return numberOfRows!
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        pickerView.subviews[1].isHidden = true
        pickerView.subviews[2].isHidden = true
        
        let label : UILabel = UILabel.init(frame: CGRect(x: 0, y: 0, width: Int(frame.size.width), height: rowHeight))
        
        label.textAlignment = .center
        label.font = UIFont (name: "HelveticaNeue-Light", size: 30)
        label.textColor = UIColor.white
        
        label.text = "\(row)"
        
        return label
    }
    
    // MARK - UIPickerViewDelegate
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return CGFloat(rowHeight)
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerType! {
        case TimePickerType.Hours:
            RehegooConfig.setHours(value: row)
        case TimePickerType.Minutes:
            RehegooConfig.setMinutes(value: row)
        case TimePickerType.Seconds:
            RehegooConfig.setSeoncds(value: row)
        }
    }
}
